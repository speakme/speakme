package mystech.speakme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class LauncherActivity extends Activity {
	@Override
    public void onCreate(Bundle savedInstanceState)
    {   
        super.onCreate(savedInstanceState);

        FileLog.getLog().setFile(getApplicationContext(), SpeakMe.LOGGING_FILE);
        startService(new Intent(this, SpeakMe.class));
        finish();
    }

}
