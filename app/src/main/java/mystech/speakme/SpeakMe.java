package mystech.speakme;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import junit.framework.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;

public class SpeakMe extends Service {

    /**
     * Holds the constant that represents all SpeakMe plugins in the system
     */
    public static final String ACTION_PICK_PLUGIN = "speak.me.action.PICK_PLUGIN";
    public static final String LOGGING_FILE = "log.file";


    // Contains messages to send to the handler
    public static final int SPEAKME_MESSAGE_START_LISTENING = 10;
    public static final int SPEAKME_MESSAGE_PAUSE_LISTENING = 11;
    public static final int SPEAKME_MESSAGE_RESET_LISTENING = 12;

    // Broadcasts will send a message to the handler from another process/app
    public static final String SPEAKME_START_LISTENING = "event-start-broadcast";
    public static final String SPEAKME_STOP_LISTENING = "event-stop-broadcast";
    public static final String SPEAKME_QUIT = "event-quit-broadcast";
    public static final String SPEAKME_SEND_ERROR = "event-send-error";

    // Arbitrary notification ID for our activity
    private static final int ONGOING_NOTIFICATION_ID = 865617;

    // Deals with Threading stuff
    private Looper serviceLooper;
    private ServiceHandler serviceHandler;

    // Contains The hashmap of discovered plugins to the commands that start them
    private HashMap<Intent, String[]> commandMap = new HashMap<Intent, String[]>();

    // Associates this Service with parent
    private Notification notification;
    private SpeechRecognizer speechService;

    public static AudioManager audioManager;
    /**
     * Defines the handler used to start/stop services as needed.
     */
    private class ServiceHandler extends Handler {

        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            FileLog.getLog().DLog("SPEAKME", "HANDLING MESSAGE: " + msg.what);
            ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = manager.getActiveNetworkInfo();

            if (activeNetworkInfo == null  || !activeNetworkInfo.isConnected()) {
                Notification n = new Notification.Builder(getApplicationContext())
                        .setContentTitle("SpeakMe Crashed")
                        .setContentText("Please enable your network.")
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setAutoCancel(true)
                        .build();

                NotificationManager notificationManager = (NotificationManager)
                        getSystemService(NOTIFICATION_SERVICE);
                notificationManager.notify(0, n);

                SpeakMe.this.stopSelf();
            }

            //Listen for the "key Words"
            switch (msg.what) {
                case SPEAKME_MESSAGE_PAUSE_LISTENING:
                    // Cancels the speech listener
                    if (speechService != null) {
                        speechService.cancel();
                    }

                    // Sets the audio to be unmuted.
                    audioManager.setStreamMute(AudioManager.STREAM_MUSIC,false);
                    //FileLog.getLog().DLog("VOLUME CONTROL PAUSE", audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) + "");
                    break;
                case SPEAKME_MESSAGE_START_LISTENING:
                case SPEAKME_MESSAGE_RESET_LISTENING:

                    if (speechService != null) {
                        speechService.cancel();
                        speechService.destroy();
                    }

                    speechService = SpeechRecognizer.createSpeechRecognizer(getApplication());
                    speechService.setRecognitionListener(new CommandListener());

                    // And then invoke it again.
                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                    intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "mystech.speakme");
                    intent.putExtra(RecognizerIntent.EXTRA_PROMPT, false);
                    intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);

                    audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
                    if (audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) != 0) {
                        audioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
                    }
                    speechService.startListening(intent);

                    //FileLog.getLog().DLog("VOLUME CONTROL RESTART", audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) + "");\
                    break;
            }

        }

    }

    @Override
    public void onCreate() {
        audioManager = (AudioManager)getApplicationContext().getSystemService(AUDIO_SERVICE);
        Log.d("SPEAKME", "Starting up Speak Me");

        // Find all plugins that have the intent filter that we have specified in ACTION_PICK_PLUGIN
        PackageManager pacMan = this.getPackageManager();
        Intent baseIntent = new Intent(ACTION_PICK_PLUGIN);

        // Query for all matching services AND activities.
        List<ResolveInfo> plugins =
                pacMan.queryIntentServices(baseIntent, PackageManager.GET_RESOLVED_FILTER);
        List<ResolveInfo> pluginsAct =
                pacMan.queryIntentActivities(baseIntent, PackageManager.GET_RESOLVED_FILTER);
        FileLog.getLog().DLog("SPEAK_ME", "SIZE: " + plugins.size() + pluginsAct.size());

        // Create a command map of Intents to commands
        for (int i = 0; i < plugins.size(); i++) {
            // Get service information
            ResolveInfo info = plugins.get(i);
            ServiceInfo sinfo = info.serviceInfo;
            try {
                FileLog.getLog().DLog("SM", "[packageName:] " + sinfo.packageName);
                FileLog.getLog().DLog("SM", "[className:] " + sinfo.name);

                // Grab the metadata
                ComponentName cn = new ComponentName(sinfo.packageName, sinfo.name);
                ServiceInfo meta = pacMan.getServiceInfo(cn, PackageManager.GET_META_DATA);

                // Grab the keywords out of the metadata
                String[] keywords = meta.metaData.getString(getResources().getString(R.string.keywords_id)).split(",");

                // Create the intent and put into the mapping.
                Intent intent = new Intent(ACTION_PICK_PLUGIN);
                intent.setComponent(cn);
                intent.putExtra("activity?", false);
                commandMap.put(intent, keywords);
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            }

        }
        // Do the same for activities
        for (int i = 0; i < pluginsAct.size(); i++) {
            ResolveInfo info = pluginsAct.get(i);
            ActivityInfo ainfo = info.activityInfo;

            try {
                FileLog.getLog().DLog("SM", "[packageName:] " + ainfo.packageName);
                FileLog.getLog().DLog("SM", "[className:] " + ainfo.name);
                ComponentName cn = new ComponentName(ainfo.packageName, ainfo.name);
                ActivityInfo meta = pacMan.getActivityInfo(cn, PackageManager.GET_META_DATA);
                String[] keywords = meta.metaData.getString(getResources().getString(R.string.keywords_id)).split(",");
                Intent intent = new Intent(ACTION_PICK_PLUGIN);
                intent.setComponent(cn);
                intent.putExtra("activity?", true);
                commandMap.put(intent, keywords);
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            }
        }

        // Our main listening for the "OK Speak Me" prompt should definitely
        // be done in the background

        serviceHandler = new ServiceHandler(this.getMainLooper());

        // Set notification bar stuff
        Intent notificationIntent = new Intent(getApplicationContext(), SpeakMeMainActivity.class);
        notificationIntent.putExtra("CommandMap", commandMap);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity
                (getApplicationContext(), 1001, notificationIntent, // 1001 is magical and somehow works
                        PendingIntent.FLAG_UPDATE_CURRENT);         // On phones AND tablets.


        // Run this app in the foreground
        notification = new Notification.Builder(this.getApplicationContext())
                .setContentTitle(getResources().getString(R.string.notification_title))
                .setContentText(getResources().getString(R.string.notification_content))
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_launcher)
                .setPriority(Notification.PRIORITY_MAX)
                .setAutoCancel(true)
//                .addAction(R.drawable.error_button, "Send Error Report", crashPendingReport)
                .build();


        // Build broadcast Receivers to start/stop the service
        startReceiver = new SpeakMeStartReciever();
        stopReceiver = new SpeakMeStopReciever();
        quitReceiver = new SpeakMeQuitReceiver();
        errorReceiver = new SpeakMeErrorReceiver();
        this.registerReceiver(startReceiver,
                new IntentFilter(SPEAKME_START_LISTENING));
        this.registerReceiver(stopReceiver,
                new IntentFilter(SPEAKME_STOP_LISTENING));
        this.registerReceiver(quitReceiver,
                new IntentFilter(SPEAKME_QUIT));
        this.registerReceiver(errorReceiver,
                new IntentFilter(SPEAKME_SEND_ERROR));


        // Make this run in the foreground.
        startForeground(ONGOING_NOTIFICATION_ID, notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Toast.makeText(this, "SpeakMe starting", Toast.LENGTH_SHORT).show();
        Log.d("SPEAKME", "OnStartCommand");
        SharedPreferences prefs = getSharedPreferences("SPEAKME_PREFS",MODE_MULTI_PROCESS);
        if (!prefs.getBoolean("Instructions Shown",false)) {
            Intent instructionIntent = new Intent(getApplicationContext(),InstructionsActivity.class);
            instructionIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(instructionIntent);
            prefs.edit().putBoolean("Instructions Shown", true).commit();
        }

        // If we get killed, after returning from here, restart
        //serviceHandler.handleMessage(Message.obtain(serviceHandler, SPEAKME_MESSAGE_START_LISTENING));
        resetSpeech();
        return START_STICKY;
    }

    /**
     * Responds to the reset broadcast
     */
    public void resetSpeech() {
        Log.d("SPEAKME", "In resetSpeech()");
        serviceHandler.handleMessage(Message.obtain(serviceHandler, SPEAKME_MESSAGE_RESET_LISTENING));
    }

    /**
     * Responds to the stop broadcast
     */
    public void stopSpeech() {
        Log.d("SPEAKME","In stopSpeech()");
        serviceHandler.handleMessage(Message.obtain(serviceHandler, SPEAKME_MESSAGE_PAUSE_LISTENING));
    }

    @Override
    public IBinder onBind(Intent arg0) {
        // This main application thread will not be bound
        return null;
    }

    @Override
    public void onDestroy() {
        //Toast.makeText(this, "SpeakMe service done", Toast.LENGTH_SHORT).show();

        // Make sure we stop the listening service so that other services can use it.
        serviceHandler.handleMessage(Message.obtain(serviceHandler, SPEAKME_MESSAGE_PAUSE_LISTENING));
        if (speechService != null) {
            speechService.cancel();
            speechService.destroy();
            speechService = null;
        }

        audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);

        // On closing make sure these broadcasts do not exist.
        unregisterReceiver(startReceiver);
        unregisterReceiver(stopReceiver);
        unregisterReceiver(quitReceiver);
        unregisterReceiver(errorReceiver);
    }

    /**
     * Responsible for ensuring that the Listening service is started.
     */
    public class SpeakMeStartReciever extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("SpeakMeStartReciever", "Reset Service");
            resetSpeech();
        }
    }
    BroadcastReceiver startReceiver = new SpeakMeStartReciever();

    /**
     * Responsible for ensuring that listening service is disabled.
     */
    public class SpeakMeStopReciever extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("SpeakMeStopReceiver", "Stop Service");
            SpeakMe.this.serviceHandler.handleMessage(
                    Message.obtain(serviceHandler, SPEAKME_MESSAGE_PAUSE_LISTENING));
        }
    }
    BroadcastReceiver stopReceiver = new SpeakMeStopReciever();

    public class SpeakMeQuitReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("SpeakMeQuitReceiver", "Quitting Service");
            stopSpeech();
            notification.contentIntent.cancel();
            stopSelf();
        }
    }
    BroadcastReceiver quitReceiver = new SpeakMeStopReciever();

    public class SpeakMeErrorReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            FileLog.getLog().sendLogReport(getApplicationContext());
        }
    }
    BroadcastReceiver errorReceiver = new SpeakMeStopReciever();

    private class CommandListener implements RecognitionListener {
        private static final String TAG = "COMMAND_LISTENER";

        @Override
        public void onReadyForSpeech(Bundle bundle) {
            // Turn on the Audio again after the beep has passed.
            Log.d("CommandListener", "OnReadyForSpeech()");
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC,false);
            //am.setStreamVolume(AudioManager.STREAM_MUSIC, 100, AudioManager.ADJUST_RAISE);
            //FileLog.getLog().DLog("VOLUME CONTROL INSIDE", SpeakMe.audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) + "");
            Assert.assertTrue("AudioManager still muted.", audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) != 0);
        }

        @Override
        public void onBeginningOfSpeech() {
            FileLog.getLog().DLog(TAG, "In onBeginningOfSpeech");
        }

        @Override
        public void onRmsChanged(float v) {
            //FileLog.getLog().DLog(TAG, "In onRmsChanged");
        }

        @Override
        public void onBufferReceived(byte[] bytes) {
            FileLog.getLog().DLog(TAG, "In onBufferReceived");
        }

        @Override
        public void onEndOfSpeech() {
            FileLog.getLog().DLog(TAG, "In onEndOfSpeech");
        }

        @Override
        public void onError(int i) {
            FileLog.getLog().DLog(TAG, "In onError");
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC,false);
            // When there is an error, indicate to the parent that we need to reset the listener.
            resetSpeech();
        }

        @Override
        public void onResults(Bundle bundle) {

            audioManager.setStreamMute(AudioManager.STREAM_MUSIC,false);
            FileLog.getLog().DLog(TAG, "In onResults");

            // Retrieve the result array (sorted in descending order by accuracy)
            ArrayList<String> results = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

            // Get the set of strings that will close the app.
            String[] closeKeywordsArray = getResources().getStringArray(R.array.quit_keywords);
            Set<String> closeKeywordsSet = new HashSet<String>();
            closeKeywordsSet.addAll(Arrays.asList(closeKeywordsArray));
            String calledName = getResources().getString(R.string.called_name);

            // Indicates whether to stop or reset the parent
            boolean speakFound = false;

            FileLog.getLog().DLog(TAG, "Called Name: " + calledName);
            FileLog.getLog().DLog(TAG, "Close Keyword Set: " + closeKeywordsSet);

            //  Iterate through the results of speech recognition and launch the Main Activity if
            // the name for the app is spoken.
            for (String result: results)  {
                result = result.toLowerCase();
                FileLog.getLog().DLog(TAG, "Results: " + result);

                // If the user wants to close the app, then intercept that here.
                if (result.contains(calledName) && itemInX(result, closeKeywordsSet)) {
                    stopSpeech();
                    stopSelf();
                    notification.contentIntent.cancel();
                    break;
                }
                // Else, launch the Activity, and make sure to explicity STOP speech. This is
                // a shared resource.
                else if ((result.contains("ok " + calledName) || result.contains("okay " + calledName)) &&
                        !itemInX(result, closeKeywordsSet)) {
                    try {
                        stopSpeech();
                        notification.contentIntent.send();
                        speakFound = true;
                        break;
                    } catch (PendingIntent.CanceledException e) {
                        e.printStackTrace();
                    }
                }
            }

            // If the keywords were not found, then reset the listener. This will allow users
            // to have normal conversation outside of Alfred.
            if (!speakFound)
                resetSpeech();
        }

        @Override
        public void onPartialResults(Bundle bundle) {
            FileLog.getLog().DLog(TAG, "In onPartialResults");
        }

        @Override
        public void onEvent(int i, Bundle bundle) {
            FileLog.getLog().DLog(TAG, "In onEvent");
        }

        /**
         * Determines whether any of several items in a set are also present in a given string.
         * @param x The string to search within
         * @param items The set of items to check on.
         * @return True/False
         */
        private boolean itemInX(String x, Set<String> items) {
            for (String item : items) {
                if (x.contains(item))
                    return true;
            }
            return false;
        }
    }
}

