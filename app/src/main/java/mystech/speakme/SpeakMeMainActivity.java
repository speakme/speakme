package mystech.speakme;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import junit.framework.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Responsible for using a more powerful speech recognition (built in) and then delegating
 * the inputted text to the appropriate plugin.
 */
public class SpeakMeMainActivity extends Activity {

    private ImageButton btnSpeak;
    private TextView currentCommandTextView;

    private boolean intentLaunched = false;

    // The same commandMap passed from SpeakMe.java
    private HashMap<Intent, String[]> commandMap;

    private SpeechRecognizer speechService;
    private Intent speechIntent;
    private CustomRecognitionListener customRecognitionListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // Retrieve the command map from the Intent extras.
        commandMap = (HashMap<Intent, String[]>) this.getIntent().getExtras().getSerializable("CommandMap");

        // Retrieve the items from xml
        currentCommandTextView = (TextView) findViewById(R.id.currentSpeech);
        btnSpeak = (ImageButton) findViewById(R.id.btnSpeak);

        // If the speech button is pressed, launch the speech recognizer activity that
        // is built into Android.
        btnSpeak.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startSpeechForeground();
            }
        });
    }
    
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main, menu);
//        menu.add("Send an error Report").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem menuItem) {
//                FileLog.getLog().sendLogReport(SpeakMeMainActivity.this);
//                return true;
//            }
//        });
//        return true;
//    }
    
    @Override
    public void onResume() {
        super.onResume();

        startSpeechForeground();
    }

    private void startSpeechForeground() {

        // Stop background service
        Intent pauseService = new Intent();
        pauseService.setAction(SpeakMe.SPEAKME_STOP_LISTENING);
        sendBroadcast(pauseService);

        Handler h = new Handler(getMainLooper());
        h.postAtFrontOfQueue(new Runnable() {
            @Override
            public void run() {
                // remove any existing service.
                if (speechService != null) {
                    speechService.cancel();
                    speechService.destroy();
                }

                speechService = SpeechRecognizer.createSpeechRecognizer(getApplication());
                customRecognitionListener = new CustomRecognitionListener();
                speechService.setRecognitionListener(customRecognitionListener);

                // Indicate we want to listen to speech and set the necessary parameters
                speechIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                speechIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                speechIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "mystech.speakme");
                speechIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);
                speechIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);

                // Start new service.
                speechService.startListening(speechIntent);
            }
        });

    }

    private void stopForegroundSpeech() {

        Handler h = new Handler(getMainLooper());
        h.postAtFrontOfQueue(new Runnable() {
            @Override
            public void run() {
                // Stop any services in the foreground
                if (speechService != null) {
                    speechService.cancel();
                    speechService.destroy();
                    speechService = null;
                }
            }
        });

        // Reset the background listener
        if (!intentLaunched) {
            Intent startService = new Intent();
            startService.setAction(SpeakMe.SPEAKME_START_LISTENING);
            sendBroadcast(startService);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        stopForegroundSpeech();
        if (customRecognitionListener.dialog != null) {
            customRecognitionListener.dialog.cancel();
        }
    }


    private class CustomRecognitionListener implements RecognitionListener {

        @Override
        public void onReadyForSpeech(Bundle bundle) {

        }

        @Override
        public void onBeginningOfSpeech() {
            currentCommandTextView.setBackgroundResource(R.drawable.rounded_corners_highlighted);
        }

        @Override
        public void onRmsChanged(float v) {

        }

        @Override
        public void onBufferReceived(byte[] bytes) {

        }

        @Override
        public void onEndOfSpeech() {
            currentCommandTextView.setBackgroundResource(R.drawable.rounded_corners);
        }

        @Override
        public void onError(int i) {
            currentCommandTextView.setBackgroundResource(R.drawable.rounded_corners);
        }

        @Override
        public void onResults(Bundle bundle) {
            // Configured to get the top 5 results from the speech.
            ArrayList<String> text = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

            if (text != null && text.size() > 0) {
                currentCommandTextView.setText(text.get(0));
                currentCommandTextView.setBackgroundColor(Color.LTGRAY);
            }

            String[] closeKeywordsArray = getResources().getStringArray(R.array.quit_keywords);

            for (String item : closeKeywordsArray) {
                if (text.get(0).equalsIgnoreCase(item)) {
                    speechService.stopListening();
                    speechService.cancel();
                    speechService.destroy();
                    Intent closeIntent = new Intent();
                    closeIntent.setAction(SpeakMe.SPEAKME_QUIT);
                    sendBroadcast(closeIntent);
                    finish();
                    return;
                }

            }

            if (text.get(0).equalsIgnoreCase("help") || text.get(0).equalsIgnoreCase("help me")) {
                Intent instructionIntent = new Intent(getApplicationContext(), InstructionsActivity.class);
                //instructionIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                instructionIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplication().startActivity(instructionIntent);
            }

            if (text.get(0).toLowerCase().contains("send") && text.get(0).toLowerCase().contains("error") &&
                    text.get(0).toLowerCase().contains("report")) {
                FileLog.getLog().sendLogReport(getApplicationContext());
            }

            LinkedList<Intent> matchList = new LinkedList<Intent>();
            for (Intent i : commandMap.keySet()) {
                for (String t : text) {
                    // count the number of matches of commandSet in t
                    int matches = 0;
                    for (String s : commandMap.get(i)) {
                        if (t.toLowerCase().contains(s.toLowerCase())) {
                            matches++;
                        }
                    }

                    // If there is a 100% match, then add it to the histogram.
                    if (matches == commandMap.get(i).length) {
                        i.putExtra("TEXT", t);
                        matchList.add(i);
                    }
                }
            }

            // Break if no results found
            if (matchList.size() == 0) {

                showDialog();
                return;
            }

            // Launch the most likely plugin (most requirements on solution)
            Intent i = matchList.get(0);
            int currLen = commandMap.get(i).length;
            for (Intent j : matchList) {
                if (commandMap.get(j).length > currLen) {
                    i = j;
                    currLen = commandMap.get(j).length;
                }
            }

            // Set when the intent was discovered.
            boolean activity = i.getBooleanExtra("activity?", false);
            if (activity) {
                Log.d("SPEAKMEMainActivity", "Starting Activity: " + i.getAction());
                stopForegroundSpeech();
                if (customRecognitionListener.dialog != null) {
                    customRecognitionListener.dialog.cancel();
                }
                // Have to specify that it is an activity in own process...
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

                intentLaunched = true;

            } else {
                Log.d("SPEAKMEMainActivity", "Starting Service: " + i.getAction());
                stopForegroundSpeech();
                if (customRecognitionListener.dialog != null) {
                    customRecognitionListener.dialog.cancel();
                }
                startService(i);

                intentLaunched = true;
            }

            finish();

        }

        @Override
        public void onPartialResults(Bundle bundle) {
            ArrayList<String> partials = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

            currentCommandTextView.setText(partials.get(0));
        }

        @Override
        public void onEvent(int i, Bundle bundle) {

        }

        public AlertDialog dialog;
        private void showDialog() {
            currentCommandTextView.setBackgroundResource(R.drawable.rounded_corners);

            AlertDialog.Builder builder = new AlertDialog.Builder(SpeakMeMainActivity.this);
            builder.setMessage("A plugin matching that statement was not found.");
            builder.setCancelable(true);
            builder.setNeutralButton(R.string.cancelText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialog.cancel();
                    dialog.dismiss();
                    stopForegroundSpeech();
                    finish();
                }
            });
            dialog = builder.show();

            Timer t = new Timer();
            t.schedule(new TimerTask() {
                @Override
                public void run() {
                    dialog.cancel();
                    dialog.dismiss();
                    stopForegroundSpeech();
                    finish();
                }
            }, 3000);

        }
    }
}




