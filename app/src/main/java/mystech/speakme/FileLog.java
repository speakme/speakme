package mystech.speakme;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by stephen on 5/25/14.
 */
public class FileLog {

    private final String DEFAULT_FILE_NAME = "log.log";

    private static FileLog log;
    private FileLog() {}

    public static FileLog getLog() {
        if (log == null) {
            log = new FileLog();
        }
        return log;
    }

    private File loggingFile;
    private PrintWriter fOut;
    private FileOutputStream outStream;

    /**
     * Call to set the initial parameters for the opened file
     * @param c Context to save into
     * @param filename name of logging file
     */
    public void setFile(Context c, String filename) {
        loggingFile = new File(c.getFilesDir(), filename);
        setPrintWriter();
    }

    private void setPrintWriter() {
        try {
            if (loggingFile == null) {
                loggingFile = new File(DEFAULT_FILE_NAME);
            }
            fOut = new PrintWriter(loggingFile);
        } catch (FileNotFoundException e) {
            Log.e("LOGGING", "internal logging file not found");
        }
    }

    public void DLog(String tag, String text) {

        Log.d(tag, text);

    }

    public void ELog(String tag, String text) {

        Log.d(tag, text);
    }

    public String readLog() {
        try {
            Scanner fIn = new Scanner(loggingFile);
            ArrayList<String> lines = new ArrayList<String>();
            while (fIn.hasNextLine()) {
                lines.add(fIn.nextLine());
            }
            fIn.close();

            fOut.close();
            fOut = null;
            return TextUtils.join("\r\n", lines
            );
        } catch (FileNotFoundException e) {
            Log.e("LOGGING", "internal logging file not found");
        }
        return "";
    }

    public boolean sendLogReport(Context c) {
        String loggingData = readLog();

        Log.d("LOGGING EMAIL", "Emailing text: " + loggingData);
        Intent i = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:mystechstudios@gmail.com"));
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"mystechstudios@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Error Log Submission");
        i.putExtra(Intent.EXTRA_TEXT, loggingData);
        i.putExtra(Intent.EXTRA_HTML_TEXT, loggingData);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        c.startActivity(i);

        return true;
    }

}
