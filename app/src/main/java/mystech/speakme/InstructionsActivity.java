package mystech.speakme;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class InstructionsActivity extends Activity {

    private WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);

        webview = (WebView) findViewById(R.id.webView);
        webview.loadUrl("file:///android_asset/introduction.html");
    }

}
